from itertools import product


class Sn:
    def __init__(self, permutation):
        self.N = len(permutation)
        self.els = permutation

    def __call__(self, number):
        return self.els[number-1]

    def __repr__(self):
        return f'[{" ".join(map(str, self.els))}]'

def swap(k):
    return [k+j for j in range(1, k+1)] + list(range(1, k+1))

def save_the_first_card(n, S):
    results = []
    for comb in product([0, 1], repeat=n//2-1):
        res = 1
        for i in range(len(comb)):
            res = S[i](res) if comb[i] else res
        if res == n//2:
            results.append(comb)
    print(f'1: {len(results)}')
    return results

def put_second_card(n, S, k, results_first_cards):
    new_results = []
    for comb in results_first_cards:
        res = k
        for i in range(len(comb)):
            res = S[i](res) if comb[i] else res
        if res == n//2 - 1:
            new_results.append(comb)
    print(f'{k}: {len(new_results)}')
    return new_results

def transform_to_numbers(results):
    """ 
    Функция для преобразованя полученных результатов в последовательность 
    количеств карт в колоде во время собирания, когда цыганка должна перемешать колоду
    """
    n = 2 * (len(results) + 1)
    return [[2*(i+1) for i, x in enumerate(comb) if x] + [n] for comb in results]

def main():
    n = 56
    # Это операции, которые может применять цыганка
    S = [Sn(swap(i) + list(range(2*i+1, n+1))) for i in range(1, n//2)]

    # Все способы получить сверху ту же карту, что и была. Задает n//2 - 1 число 0/1.
    results = save_the_first_card(n, S)
    # Способы получить i-ую на втором месте
    for i in range(2, n+1):
        new_results = put_second_card(n, S, i, results)

if __name__ == '__main__':
    main()



import os
from math import inf

from permutations import Sn


class SchreirNode:
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return f'<{self.value}>'

    def __repr__(self):
        return f"SchreirNode({self.value})"

    def __hash__(self):
        return hash(f'SchreirNode({self.value})')
    
    def __eq__(self, node):
        return self.value == node.value


class SchreirTree:
    def __init__(self, S, b):
        self.root = SchreirNode(b)
        self.S = S
        self.N = S[0].N
        self.data = ""
        self.orbit = {self.root.value: Sn([], self.N)}

        self.build(self.root.value)

    @property
    def size(self):
        return len(self.orbit)

    def build(self, v):
        for s in self.S:
            if s(v) not in self.orbit:
                self.orbit[s(v)] = s * self.orbit[v]
                self.data += f'"{s(v)}" -> "{v}" [ label = "{s.to_circ}" ]\n'
                self.build(s(v))

    def main_build(self):
        self.data = 'digraph {\n'

        self.build(self.root.value)

        self.data += "}"
        with open(f'trees/tree_{self.root.value}.dot', 'w') as fout:
            print(self.data, file=fout)
        os.system(f'dot trees/tree_{self.root.value}.dot -Tpng > trees/SchreirTree_{self.root.value}.png')


if __name__ == '__main__':
    S = [Sn([1, 2], 5), Sn([2, 3], 5), Sn([3, 4], 5), Sn([3, 5], 5)]
    tree = SchreirTree(S, 1)

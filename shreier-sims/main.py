from pprint import pprint

from chain_stabilizers import ChainStabilizers
from permutations import Sn 


def swap(k):
    return [k+j for j in range(1, k+1)] + list(range(1, k+1))

def main():
    n = 56
    S = [Sn(swap(i) + list(range(2*i+1, n+1)), n, False) for i in range(1, n//2 + 1)]
    base = list(range(1, n+1))
    print(f'base: {base}')
    print(f'S: {S}')
    stabilizers = ChainStabilizers(n, base, S)
    print("Stabilizers +")
    stabilizers.make_all_generators()
    stabilizers.check_chain_stabilizers()

    # Теперь можно проверять, принадлежит ли перестановка группе
    assert(stabilizers.check_containing(S[0])) # эта перестановка точно должна быть, так как является образующей


if __name__ == '__main__':
    main()

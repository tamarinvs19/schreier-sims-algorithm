import itertools as it


class Sn:
    def __init__(self, permutation, N=0, is_circ=True):
        self.N = max(N, len(permutation))
        if len(permutation) == 0:
            els = list(range(1, N+1))
        elif is_circ:
            els = [i for i in range(1, self.N+1)]
            for i, x in enumerate(permutation):
                els[x-1] = permutation[(i+1) % len(permutation)]
        else:
            els = permutation
        self.els = els

    @property
    def to_circ(self):
        res = []
        circ = [1]
        while sum(len(c) for c in res) < self.N:
            next_num = self.els[circ[-1]-1]
            if next_num == circ[0]:
                res.append(circ)
                not_used = [i for i in range(1, self.N+1) if all(i not in c for c in res)]
                if len(not_used) > 0:
                    circ = [not_used[0]]
            else:
                circ.append(next_num)
        
        result =  ''.join([f'({" ".join(map(str, circ))})' for circ in res if len(circ) > 1])
        if result == '':
            result = 'e'
        return result

    def __mul__(self, second):
        res = [i for i in range(1, self.N+1)]
        for i in range(self.N):
            res[i] = self.els[second.els[i]-1]
        return Sn(res, self.N, False)

    def __invert__(self):
        return Sn([self.els.index(i+1)+1 for i in range(self.N)], self.N, False)

    def __call__(self, value):
        return self.els[value-1]

    def __eq__(self, value):
        return self.els == value.els

    def __str__(self):
        return f"Sn({self.to_circ}, {self.N})"
        # return f'[{" ".join(map(str, self.els))}]'

    def __repr__(self):
        return f'[{" ".join(map(str, self.els))}]'

    def __hash__(self):
        return hash(str(self))

if __name__ == '__main__':
    a = Sn([3, 4, 2], 5)
    b = Sn([1, 2], 5)
    print(a)
    print(a.to_circ)
    print((~a).to_circ)
    print((a*b).to_circ)
    print((b*a).to_circ)
    print(a(4))
    print((~a)(4))

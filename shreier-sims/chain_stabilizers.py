from functools import reduce

from schreir_tree import SchreirTree
from permutations import Sn


class ChainStabilizers:
    def __init__(self, N, base, S):
        self.N = N
        self.base = base
        self.S = S
        self.trees = [SchreirTree(S, b) for b in base]
        self.generators = {0: S}

    def get_order(self):
        return reduce(lambda a, b: a*b, [tree.size for tree in self.trees], 1)

    def get_orbit(self, i):
        return self.trees[i-1].orbit

    def make_generator(self, S, i):
        orbit = self.get_orbit(i)
        new_S = []
        for s in S:
            for v in orbit:
                new_S.append((~orbit[s(v)])*s*orbit[v])
        self.generators[i] = new_S

    def make_all_generators(self):
        for i in range(1, self.N+1):
            self.make_generator(self.generators[i-1], i)

    def check_chain_stabilizers(self):
        res = all(not all(x[0](self.base[i-1]) == self.base[i-1] for i in range(1, self.N+1)) for x in self.generators.values())
        return res

    def check_containing(self, g, i=0):
        if i == self.N:
            return g == Sn([], self.N)
        if all(g(b) == b for b in self.base[:i]):
            u = g(self.base[i])
            if u not in self.get_orbit(i):
                return False
            else:
                hu = self.trees[i].orbit[u]
                return self.check_containing(hu*g, i+1)
        else:
            return False

